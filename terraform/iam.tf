resource "aws_iam_role" "cnsapp-CloudWatchAgentServerRole-2" {
  name               = "cnsapp-CloudWatchAgentServerRole-2"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_iam_instance_profile" "cnsapp-CloudWatchAgentServerRole-instanceprofile" {
  name = "cnsapp-CloudWatchAgentServerRoleProfile"
  role = aws_iam_role.cnsapp-CloudWatchAgentServerRole-2.name
}

resource "aws_iam_role_policy" "cnsapp-CloudWatchAgentServerRole-policy" {
  name   = "cnsapp-CloudWatchAgentServerPolicy"
  role   = aws_iam_role.cnsapp-CloudWatchAgentServerRole-2.id
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "cloudwatch:PutMetricData",
                "ec2:DescribeTags",
                "logs:PutLogEvents",
                "logs:DescribeLogStreams",
                "logs:DescribeLogGroups",
                "logs:CreateLogStream",
                "logs:CreateLogGroup"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ssm:GetParameter"
            ],
            "Resource": "arn:aws:ssm:*:*:parameter/AmazonCloudWatch-*"
        }
    ]
}
EOF

}

