# Internet VPC
resource "aws_vpc" "cnsapp-main" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"
  tags = {
    Name = "cnsapp-main"
  }
}

# Subnets
resource "aws_subnet" "cnsapp-main-public-1" {
  vpc_id                  = aws_vpc.cnsapp-main.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "eu-west-1a"

  tags = {
    Name = "cnsapp-main-public-1"
  }
}

resource "aws_subnet" "cnsapp-main-private-1" {
  vpc_id                  = aws_vpc.cnsapp-main.id
  cidr_block              = "10.0.21.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "eu-west-1a"

  tags = {
    Name = "cnsapp-main-private-1"
  }
}

resource "aws_subnet" "cnsapp-main-private-2" {
  vpc_id                  = aws_vpc.cnsapp-main.id
  cidr_block              = "10.0.22.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "eu-west-1b"

  tags = {
    Name = "cnsapp-main-private-2"
  }
}

# Internet GW
resource "aws_internet_gateway" "cnsapp-main-gw" {
  vpc_id = aws_vpc.cnsapp-main.id

  tags = {
    Name = "cnsapp-main"
  }
}

# route tables
resource "aws_route_table" "cnsapp-main-public" {
  vpc_id = aws_vpc.cnsapp-main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.cnsapp-main-gw.id
  }

  tags = {
    Name = "cnsapp-main-public-1"
  }
}

# route associations public
resource "aws_route_table_association" "cnsapp-main-public-1-a" {
  subnet_id      = aws_subnet.cnsapp-main-public-1.id
  route_table_id = aws_route_table.cnsapp-main-public.id
}

