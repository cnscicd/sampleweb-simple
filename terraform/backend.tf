terraform {
  backend "s3" {
    bucket = "cns-node-aws-jenkins-terraform"
    key    = "cnsapp-node-aws-jenkins-terraform.tfstate"
    region = "eu-west-1"
  }
}

